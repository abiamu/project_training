###popitem() in dictonary
phonelist = {"name" : "micromax","version" : 5.1,"RAM" : 3.0}
phonelist.popitem()
print(phonelist)
###len() in dictionary
phonelist = {"name" : "micromax","version" : 5.1,"RAM" : 3.0}
print(len(phonelist))
###pop() in dictionary
phonelist = {"name" : "micromax","version" : 5.1,"RAM" : 3.0}
phonelist.pop("version")
print(phonelist)
###del in dictionary
phonelist = {"name" : "micromax","version" : 5.1,"RAM" : 3.0,"camera" : "backend"}
del phonelist["name"]
print(phonelist)
###copy in dictionary
phonelist = {"name" : "micromax","version" : 5.1,"RAM" : 3.0}
x = dict(phonelist)
print(x)
###clear in dictionary
phonelist = {"name" : "micromax","version" : 5.1,"RAM" : 3.0}
phonelist.clear()
print(phonelist)



