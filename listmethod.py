###Remove list
python = ["class","variable","method"]
python.remove("method")
print(python)
###pop list
namelist = ["cheenu","meenu","parthi"]
namelist.pop()
print(namelist)
###delete list
carlist = {"Name" : "BMW","speed" : 1500,"wheel" : "four"}
del carlist["Name"]
print(carlist)
###append method using in two join list
a = ["a","b","c"]
b = [1,2,3]
for x in b:
	a.append(x)
	print(a)
###extend
x = ["a","i","o","u"]
y = [3,4,5]
x.extend(y)
print(x)
###copy()
c = ["x","y","z"]
d = c.copy()
print(d)


