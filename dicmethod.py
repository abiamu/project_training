###dictionary 
#access the value in dictionary
carlist = {"carname" : "BMW","brand" : "ford","model" : "mustang","year" : 1995}
x = carlist.get("model")
print(x)
###change value in dictionary
carlist = {"carname" : "BMW","brand" : "ford","model" : "mustang","year" : 1995}
carlist["year"] = 1990
print(carlist)
###Add items in dictionary
carlist = {"carname" : "BMW","brand" : "ford","model" : "mustang","year" : 1995}
carlist["color"] = "red"
print(carlist)
###loop in dictionary(O/P display in key only)
carlist = {"carname" : "BMW","brand" : "ford"}
for x in carlist:
	print(x)
###values in dictionary(O/P display in values only
carlist = {"model" : "mustang","year" : 1995}
for x in carlist:
	print(carlist[x])
###items in dictionary
carlist = {"carname" : "BMW","brand" : "ford","model" : "mustang","year" : 1995}
for x,y in carlist.items():
	print(x,y)


