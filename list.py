###Range of index list
fruits = ["apple","banana","cherry","strewberry","kiwi","mango"]
print(fruits[3:6])
###change item value
fruits = ["apple","banana","cherry","strewberry","kiwi","mango"]
fruits[-1] = "orange"
print(fruits)
###length in list
fruits = ["apple","banana","cherry","strewberry","kiwi","mango"]
print(len(fruits))
###loop in list
fruits = ["apple","banana","cherry","strewberry","kiwi","mango"]
for x in fruits:
	print(x)
###insert list
fruits = ["apple","banana","cherry","strewberry","kiwi","mango"]
fruits.insert(1, "bineapple")
print(fruits)


