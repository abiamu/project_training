###working with string in join
python_tuple = ("python class","python method")
###join command using in tuple
x = " concept,".join(python_tuple)
print(x)
print(type(python_tuple))
###dictionary
python_dict = {"name" : "abi","age" : "25"}
x = " dob ".join(python_dict)
print(x)
print(type(python_dict))
###list
python_list = ["mango","orange","apple"]
x = " bineapple ".join(python_list)
print(x)
print(type(python_list))


