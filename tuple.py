##Tuple python program
tuplelist = ("apple","banana","cherry","kiwi","mango","strewberry")
print(tuplelist[3])
#nagative indexvalue and range of tuple
tuplelist = ("apple","banana","cherry","kiwi","mango","strewberry")
print(tuplelist[-4:-1])
#change the value of tuple
tuplelist = ("apple","banana","cherry","kiwi","mango","strewberry")
#convert to list in tuple value
y = list(tuplelist)
y[3] = "melon"
tuplelist = tuple(y)
print(tuplelist)
#Cretae with tuple in one item
x = ("chilli",)
print(type(x))
#length of tuple value
tuplelist = ("apple","banana","cherry","kiwi","mango","strewberry")
print(len(tuplelist))
#join two tuples:
x = ("a","b","c")
y = (1,2,3)
z = x + y
print(z)
#tuple constructor
tuplelist = (("apple","banana","cherry","kiwi","mango","strewberry"))
print(tuplelist)
#tuple remove item
tuplelist = ("apple","banana","cherry","kiwi","mango","strewberry")
del tuplelist
print(tuplelist)
