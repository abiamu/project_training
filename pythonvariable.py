###Data type
python_int = 3
python_float = 2.5
python_str = "hello python"
python_complex = 2j
#convert from int to str
A = str(python_int)
print(A)
#convert from float to int
B = int(python_float)
print(B)
#convert from int to complex
C = complex(python_complex)
print(C)

