x = "executed"
def funcname():
	x = "calculated"
	print("final answer"+x)
funcname()
print("final answer"+x)


x = "world"

def fname():
	global x # USING GLOBAL KEYWORD
	
x = "Murugan"
fname()
print("My father name is a" +x)

x = "hello world"
#display the x value
print(x)
#display the data type
print(type(x))

x = 9.5
print(type(x))

x = True
print(type(x))
 
def funame():
	x = False
#display the x value
print(x)
#display the boolean value
print(type(x))
funame()

x = "number"
def int():
	print("integer"+x)
#display the x value
print(x)
#display the integer value
print(type(x))
int()


